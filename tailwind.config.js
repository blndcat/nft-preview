module.exports = {
  content: [
		"./index.html",
	],
  theme: {
    extend: {
			fontFamily: {
				'outfit': ['Outfit', '"sans-serif"']
			},
			colors: {
				main_blue: 'hsl(217, 54%, 11%)',
				card_blue: 'hsl(216, 50%, 16%)',
				line_blue: 'hsl(215, 32%, 27%)',
				soft_blue: 'hsl(215, 51%, 70%)',
				nft_cyan: 'hsl(178, 100%, 50%)',
			},
		},
  },
  plugins: [],
}
